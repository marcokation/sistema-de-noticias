# sistema de noticias

Sistema gestor de noticias desarrollado con PHP, composer y programacion orientada a objeto (POO)

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo o pruebas.

- Instalar composer en tu equipo local
- Crear base de datos en phpMyAdmin
- Configurar phinx.yml
- Migrate database con phinx - 
```
vendor\bin\phinx migrate -e development
```

### Pre-requisitos

Para poder correr el proyecto en tu equipo local necesitas.

- PHP 7
- Composer
- XAMPP ó WampServer

## Autores

* **Marco Antonio De la cruz Santos**