<?php

/**
 * @author Marco Antonio De la cruz Santos
 * @copyright Kation Interactive 2020
 */

/**
 * AutoLoad
 * Está destinado a cargar de forma automática las clases utilizadas
 * @return string
 */
spl_autoload_register( function( string $NombreClase ) {
    require_once $NombreClase . '.php';
});

//Instancia de la clase Registro
$registro = new Registro($_POST['name'],$_POST['lastname'],$_POST['email'],$_POST['password']);
$registro->registro();

class Registro
{

    /**
    * @access public
    * @var string
    */

    public $name;
    public $lastname;
    public $email;
    public $password;

    /**
     * Constructor que recibe como parametros los datos enviados via POST
     * 
     * @access public
     * @param string $name, $lastname, $email, $password
     * 
     */
    public function __construct(string $name, string $lastname,string $email, string $password)
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = password_hash($password,PASSWORD_BCRYPT);
    }

    /**
     * Insertar un nuevo registro de usuario a la Base De Datos
     * 
     * @access public
     * @return string
     * 
     */
    public function registro(): string
    {
        //Instancia de la conexion a la BD
        $conn = new ConexionnMySQL;
        $conn->connection();

        //Nuevo registro a la BD
        $query = "INSERT INTO users (`first_name`,`last_name`,`email`,`pass`) VALUES (:first_name,:last_name,:email,:pass)";
        
        $bindParam = array("first_name"=>$this->name, "last_name"=>$this->lastname, "email"=>$this->email, "pass"=> $this->password);
        $conn->ExecuteQuery($query,$bindParam);

        return $conn->closeConexion();
    }

}