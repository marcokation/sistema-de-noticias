<?php

/**
 * @author Marco Antonio De la cruz Santos
 * @copyright Kation Interactive 2020
 */


/**
 * AutoLoad
 * Está destinado a cargar de forma automática las clases utilizadas
 * @return string
 */
spl_autoload_register( function( string $NombreClase ) {
    require_once $NombreClase . '.php';
});

//Instancia de la clase
if(isset($_POST['titulo']) && isset($_POST['autor']) && isset($_POST['contenido'])){
    
    //Instancia de la clase Noticias
    $noticia = new Noticia($_POST['titulo'],$_POST['autor'],$_POST['contenido']);

    if(isset($_POST['slug'])){
        $noticia->updateNoticia($_POST['slug']);
    }else{
        $noticia->createNoticia($_POST['userId']);
    }
}

if(isset($_POST['getSlug'])){
    //Instancia de la clase Noticias
    $noticia = new Noticia;
    $noticia->deleteNoticia($_POST['getSlug']);
}

class Noticia
{

    /**
    * @access public
    * @var string
    */

    public $titulo;
    public $contenido;
    public $autor;
    public $vistas;
    public $conn;

    /**
     * Constructor que recibe como parametros los datos enviados via POST
     * 
     * El constructor recibe como parametros los datos de la Notica via POST 
     * y una nueva instancia de la clase ConexionnMySQL
     * 
     * @access public
     * @param string $titulo, $autor, $contenido
     * 
     */
    public function __construct(string $titulo = null, string $autor = null,string $contenido = null)
    {
        $this->titulo = $titulo;
        $this->autor = $autor;
        $this->contenido = $contenido;
        $this->conn = new ConexionnMySQL;
    }

    /**
     * Retorna todas las noticias publicadas de un usuario en especifico
     * 
     * Esta función retorna todas las noticias 
     * publicadas del usuario loguedo
     * 
     * @access public
     * @param integer $userId
     * @return string
     * 
     */
    public function getNoticias(int $userId): string
    {   
        $this->conn->connection(); 

        $query = "SELECT * FROM noticias WHERE `user_id`=:userId ORDER BY id DESC";

        $bindParam = array("userId" => $userId);
        $result= $this->conn->ExecuteQuery($query,$bindParam);

        if($result[1] > 0){
            foreach ($result[0] as $row) {
                echo '<tr>
                    <th scope="row">'. $row['id'].'</th>
                    <td>'.$row['titulo'].'</td>
                    <td>'.substr($row['contenido'],0,100).'...</td>
                    <td>'.$row['autor'].'</td>
                    <td>'.$row['created_at'].'</td>
                    <td>'.$row['updated_at'].'</td>
                    <td>
                    <div class="btn-group">
                        <a href="../pages/show-noticia.php?slug='.$row['slug'].'" class="btn btn-info">Ver</a>
                        <a href="../pages/edit-noticia.php?slug='.$row['slug'].'" class="btn btn-warning">Editar</a>
                        <a href="#" onclick=" var result = confirm(`¿Seguro que deseas eliminar esta noticia?`);
                        if (result) { EliminarNoticia(`'.$row['slug'].'`) }" class="btn btn-danger">Eliminar</a>
                    </div>
                    </td>
                </tr>';
            }
            return true;
        }else{
            return '<tr>
            <td colspan="7">
            <div class="alert" role="alert">
                <h4 class="alert-heading">Sin noticias!</h4>
                <p>Aun no haz publicado ninguna noticia.</p>
                <hr>
                <p class="mb-0">Publica una nueva noticia... <a href="create-noticia.php">Pulsa aqui</a></p>
            </div>
            <td>
        <tr>';
        }
    }

    /**
    * Crear un nuevo registro de Noticias
    * 
    * Insertar en la base de datos un nuevo registro
    * de noticias con el Id del usuario loguedo
    *
    * @access public
    * @param integer $userId
    * @return string
    * 
    */
    public function createNoticia(int $userId): string
    {
        $slug = $this->titulo.rand(1,1000);
        $query = "INSERT INTO noticias (`titulo`, `contenido`, `autor`, `user_id`, `slug`) VALUES (:titulo,:contenido,:autor,:userid,:slug)";

        try{
            $this->conn->connection();

            $bindParam = array("titulo"=>$this->titulo, "contenido"=>$this->contenido, "autor"=>$this->autor, "userid"=>$userId, "slug"=> $slug);
            $this->conn->ExecuteQuery($query,$bindParam);

        }catch(Exception $e){
           echo $e->getMessage();
        }

        header("location:../pages/inicio.php");

        return true;
    }

    /**
    * Mostrar noticia
    *
    * Mostrar detalles de una noticia por individual
    *
    * @access public
    * @param string $slug
    * @return array
    * 
    */
    public function showNoticia(string $slug): array
    {
        $this->conn->connection();

        $query= "SELECT * FROM noticias WHERE slug=:slug"; 
        
        $bindParam = array("slug" => $slug);
        $result = $this->conn->ExecuteQuery($query,$bindParam);

        return $result[0][0]; 
    }

    /**
    * Actualizar noticia
    *
    * Actualizar los datos de una noticia
    *
    * @access public
    * @param string $slug
    * 
    */
    public function updateNoticia(string $slug): void
    {
        $thisNoticia = $this->showNoticia($slug);
        $newSlug = $thisNoticia['slug'];

        //Actualizar el Slug si se cambia el titulo de la noticia
        if($thisNoticia['titulo'] != $this->titulo){
            $newSlug = $this->titulo.rand(1,1000);   
        }

        $query = "UPDATE noticias SET `titulo`=:titulo,`contenido`=:contenido,`autor`=:autor,`slug`=:newSlug WHERE `slug`=:slug";
        $bindParam = array("titulo" => $this->titulo, "contenido" => $this->contenido, "autor" => $this->autor, "newSlug" => $newSlug, "slug" => $slug);
        
        $this->conn->connection();
        $this->conn->ExecuteQuery($query,$bindParam); 
        
        header("location:../pages/show-noticia.php?slug=$newSlug");
    }

    /**
    * Eliminar noticia
    *
    * Mostrar detalles de una noticia por individual
    *
    * @access public
    * @param string $slug
    * 
    */
    public function deleteNoticia(string $slug): void
    {
        $this->conn->connection();

        $query = "DELETE FROM `noticias` WHERE slug=:slug";

        $bindParam = array("slug" => $slug);
        $this->conn->ExecuteQuery($query,$bindParam);

        header("location:../pages/inicio.php"); 
    }

    /**
     * Filtro de busqueda
     * Retorna algun registro en concreto solicitado por le usuario.
     * 
     * @param integer userId
     * @param string $titulo, $contenido, $autor, $fecha 
     * @return string
     */
    public function searchNoticia(int $userId, string $titulo = null, string $contenido = null, string $autor = null, string $fecha = null): string
    {
        $this->conn->connection(); 

        $bindParam = array("user_id" => $userId, "titulo" => $titulo, "contenido" => $contenido, "autor" => $autor, "created_at" => $fecha);
        $result= $this->conn->ExecuteQuery($query = null,$bindParam);

        if($result[1] > 0){
            foreach ($result[0] as $row) {
                echo '<tr>
                    <th scope="row">'. $row['id'].'</th>
                    <td>'.$row['titulo'].'</td>
                    <td>'.substr($row['contenido'],0,100).'...</td>
                    <td>'.$row['autor'].'</td>
                    <td>'.$row['created_at'].'</td>
                    <td>'.$row['updated_at'].'</td>
                    <td>
                        <div class="btn-group">
                            <a href="../pages/show-noticia.php?slug='.$row['slug'].'" class="btn btn-info">Ver</a>
                            <a href="../pages/edit-noticia.php?slug='.$row['slug'].'" class="btn btn-warning">Editar</a>
                            <a href="#" onclick=" var result = confirm(`¿Seguro que deseas eliminar esta noticia?`);
                            if (result) { EliminarNoticia(`'.$row['slug'].'`) }" class="btn btn-danger">Eliminar</a>
                        </div>
                    </td>
                </tr>';
            }
        }else{
            return '<tr>
                <td colspan="7">
                    <div class="alert" role="alert">
                        <h4 class="alert-heading">Sin resultados!</h4>
                        <p>Owww, La busqueda que haz realizado no ha tenido resultados.</p>
                        <hr>
                        <p class="mb-0">Realiza la busqueda nuevamente.</p>
                    </div>
                <td>
            <tr>';
        }
    }

    /**
    * Obtener fechas de noticias
    *
    * Obtener las fechas de creacion de las noticias
    * para desplegarlas en el select del filtro de busqueda.
    *
    * @access public
    * @param int $userId
    * @return boolean
    * 
    */
    public function getFechas(int $userId): int
    {
        $this->conn->connection(); 

        $query = "SELECT DISTINCT DATE_FORMAT(created_at, '%Y-%m-%d') AS created_at FROM noticias WHERE `user_id`=:userId";

        $bindParam = array("userId" => $userId);
        $result= $this->conn->ExecuteQuery($query,$bindParam);

        if($result[0]){
            foreach ($result[0] as $row) {
                echo '<option value="'.$row['created_at'].'">'.$row['created_at'].'</option>';
            }
        } 
        return true;
    }
}

?>