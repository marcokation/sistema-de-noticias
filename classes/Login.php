<?php

/**
 * @author Marco Antonio De la cruz Santos
 * @copyright Kation Interactive 2020
 */

/**
 * AutoLoad
 * Está destinado a cargar de forma automática las clases utilizadas
 * @return string
 */
spl_autoload_register( function( string $NombreClase ) {
    require_once $NombreClase . '.php';
});

//Instancia de la clase
$login = new Login($_POST['email'],$_POST['password']);
$login->getLogin();


class Login
{

    /**
    * @access public
    * @var string
    */
    public $email;
    public $password;

    /**
     * Constructor que recibe como parametros los datos enviados via POST
     * 
     * @access public
     * @param string $email, $password
     * 
     */
    public function __construct(string $email, string $password)
    {
        $this->email = trim($email);
        $this->password = trim($password);
    }

    /**
     * Login de usuario
     * 
     * Comparacion del password via hash, inicializacion
     * de la variable de session, redireccionamiento a la vista
     * y retorno del cierre de conexion a la Base De Datos.
     * 
     * @access public
     * @param string $email, $password
     * @return string
     * 
     */
    public function getLogin(): string 
    {
        //Instancia de la clase
        $conn = new ConexionnMySQL;
        $conn->connection();

        $query = "SELECT * FROM users WHERE `email`=:email";
        
        //Enviar consulta y parametros a la funcion ExecuteQuery
        $bindParam = array("email" => $this->email);
        $result= $conn->ExecuteQuery($query,$bindParam);
        
        if($result[1] > 0){

            $userSession = array();

            foreach ($result[0] as $row) {
                $password_hash = $row['pass'];
            }

            if(password_verify($this->password,$password_hash)){

                session_start();
                $_SESSION['user'] = $result[0][0]; 
                
                header("location:../pages/inicio.php");
            }else{
                header("location:../index.php");
            }
        }else{
            header("location:../index.php");
        }

        return $conn->closeConexion();
    }
}