<?php

/**
 * @author Marco Antonio De la cruz Santos
 * @copyright Kation Interactive 2020
 */

class ConexionnMySQL
{

    /**
    * @access protected
    * @var string
    */
    private $host;
    private $user;
    private $password;
    private $db;
    private $conexion;
   
    public function __construct()
    { 
        $this->host="localhost";
        $this->user="root";
        $this->password="";
        $this->db="mctekk";
    }

    /**
    * Conexion a la base de datos
    * 
    * @access public
    */
    public function connection(): void
    {
        try{
            $this->conexion = new PDO('mysql:host=localhost;dbname=mctekk', $this->user, $this->password);
            $this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo 'Falló de conexión: ' . $e->getMessage();
        }
    }

    /**
     * Cierre de conexion mysql
     * 
     * @access public
     */
    public function closeConexion(): void
    {
        $this->conexion = null;
    }

    /**
     * Retorna un query de consulta de la Base de datos
     * 
     * @access public
     * @param string $sql
     * @param array $array
     * @return string
     * 
     */
    public function ExecuteQuery($query,$array)
    {
        $sth = $this->conexion->prepare($query);

        if(is_array($array)){
            foreach ($array as $key => $value) {
               $sth->bindParam(':'.$key, $value);
            }
        }

        $return = explode(' ',$query);

        //Verificar si el Query es un INSERT, UPDATE o SELECT
        if($return[0] == "SELECT"){

            $sth->execute();
            return array($sth->fetchAll(PDO::FETCH_ASSOC),$sth->rowCount());
            
        }else{
           return $sth->execute($array);
        }
    }

    /**
     * Cierre de session del usuario
     * 
     * @access public
     */
    public function closeSession(): void
    {
        session_destroy();
        header('location:../index.php');
    }

}
