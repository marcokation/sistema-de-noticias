<?php

use Phinx\Migration\AbstractMigration;

class AddNoticiasTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $noticias = $this->table('noticias');
        $noticias->addColumn('titulo', 'string', ['limit' => 190])
              ->addColumn('contenido', 'text')
              ->addColumn('autor', 'string', ['limit' => 190])
              ->addColumn('user_id', 'integer')
              ->addColumn('vistas', 'integer',['null' => true])
              ->addColumn('slug', 'string', ['limit' => 190])
              ->addColumn('created_at', 'timestamp', ['default' => 'CURRENT_TIMESTAMP'])
              ->addColumn('updated_at', 'timestamp',  ['default' => 'CURRENT_TIMESTAMP'], ['null' => true])
              ->create();
    }
}
