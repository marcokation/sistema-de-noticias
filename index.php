<!DOCTYPE html>
<html lang="en">
<head>
    <?php
    session_start();
    if($_SESSION){ header("location:pages/inicio.php"); }
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="assets/css/bootstrap.css">
    <title>Document</title>
</head>
<body>
    <div style="text-align: center;">
        <h1><u>Sistema de noticias</u></h1><br><br>
        <h2>Login</h2>
        <form action="classes/login.php" method="POST" id="login">
            <input type="text" name="email" id="email" value="" placeholder="Email"><br>
            <small id="d-email" style="color:red; display:none;">El campo email es requerido</small><br>
            <input type="password" name="password" id="password" value="" placeholder="Password"><br>
            <small id="d-pass" style="color:red; display:none;">El campo password es requerido</small><br><br>
            <button type="button" class="btn btn-success" id="entrar">Entrar</button>
        </form>
        <br>
        <p>¿Aun no tienes una cuenta? <a href="pages/registro.php">Registrarme</a></p>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="assets/js/javascript.js"></script>
</body>
</html>