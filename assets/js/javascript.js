$(document).ready(function(){
    $('#entrar').click(function(){
        if($('#email').val() == '' && $('#password').val() != ''){
            $('#d-email').css({"display":"block"});
            $('#d-pass').css({"display":"none"});
            return false;
        }else if($('#password').val() == '' && $('#email').val() != ''){
            $('#d-email').css({"display":"none"});
            $('#d-pass').css({"display":"block"});
            return false;
        }else if($('#password').val() == '' && $('#email').val() == ''){
            $('#d-email').css({"display":"block"});
            $('#d-pass').css({"display":"block"});
           return false;
        }else{
            $('#login').submit();
        }
    });
});

function EliminarNoticia(slug)
{
   $.ajax({
      type:'POST',
      url:'../classes/Noticia.php',
      data:{'getSlug': slug},
      success:function(){
        window.location = window.location.origin + '/Mctekk/pages/inicio.php';
      }
   });
}