<?php include_once('app-header.php'); ?>

<h1>
   <form method="post">
      Bienvenido <span class="text-danger"><?php echo $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name']; ?></span>! 
      <?php if(isset($_GET['searchButton'])){ echo '<a href="inicio.php" class="btn btn-secondary">Home</a>'; } ?>
      
      <input type="submit" class="btn btn-secondary" value="Cerrar sesion" name="salir" id="salir">
   </form>

   <?php 
   if(isset($_POST['salir'])){
      $close = new ConexionnMySQL;
      $close->closeSession();
   }
   ?>
</h1>
<div>
<?php
$conexion = new ConexionnMySQL;
$conexion->connection();
?>
</div>
<div class="pt-5 pb-3">
   <a href="create-noticia.php" class="btn btn-primary">+ Nueva noticia</a>
</div>
<hr>
<form method="GET">
   <div class="form-row">
      <div class="form-group col-md-3">
         <input type="text" class="form-control" name="titulo" placeholder="Titulo">
      </div>
      <div class="form-group col-md-3">
         <input type="text" class="form-control" name="contenido" placeholder="Contenido">
      </div>
      <div class="form-group col-md-3">
         <input type="text" class="form-control" name="autor" placeholder="Autor">
      </div>
      <div class="form-group col-md-2">
         <select class="form-control" name="fecha">
            <option value="">-- Seleccione una fecha --</option>
            <?php
               $nt = new Noticia; 
               echo $nt->getFechas($_SESSION['user']['id']);
            ?>
         </select>
      </div>
      <div class="form-group col-md-1">
         <button type="submit" name="searchButton" class="btn btn-success">Buscar</button>
      </div>
   </div>   
</form>
<table class="table table-striped">
   <thead>
      <tr>
         <th scope="col">#</th>
         <th scope="col">Titulo</th>
         <th scope="col">Contenido</th>
         <th scope="col">Autor</th>
         <th scope="col">Fecha de creación</th>
         <th scope="col">Fecha de actualización</th>
         <th scope="col">Acciones</th>
      </tr>
   </thead>
   <?php
      if(isset($_GET['searchButton'])){
         echo $nt->searchNoticia($_SESSION['user']['id'],$_GET['titulo'],$_GET['contenido'],$_GET['autor'],$_GET['fecha']);
      }else{
         $nt->getNoticias($_SESSION['user']['id']);
      }
   ?>
</table>

<?php include_once('app-footer.php'); ?>