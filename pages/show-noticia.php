<?php include_once('app-header.php'); ?>

<h1>
   <form method="post">
        Bienvenido <span class="text-danger"><?php echo $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name']; ?></span>!
        <a href="inicio.php" class="btn btn-secondary">Home</a>
        <input type="submit" class="btn btn-secondary" value="Cerrar sesion" name="salir" id="salir">
   </form>

   <?php 
    if(isset($_POST['salir'])){
        $cn = new ConexionnMySQL;
        $cn->closeSession();
    }

    $slug = $_GET['slug'];
    $nt = new Noticia;
    $noticia = $nt->showNoticia($slug);
   ?>
</h1>
<hr><br>
<div class="card text-center">
  <div class="card-header">
    <h3><?php echo $noticia['titulo']; ?></h3>
  </div>
  <div class="card-body">
    <h5 class="card-title">Contenido</h5>
    <p class="card-text"><?php echo $noticia['contenido']; ?></p>
    <a href="<?php echo "../pages/edit-noticia.php?slug=".$noticia['slug']; ?>" class="btn btn-warning">Editar</a>
    <a href="#" onclick=" var result = confirm(`¿Seguro que deseas eliminar esta noticia?`);
    if (result) { EliminarNoticia('<?php echo $noticia['slug']; ?>') }" class="btn btn-danger">Eliminar</a>
  </div>
  <div class="card-footer text-muted">
    <?php echo $noticia['created_at']; ?>
  </div>
</div>

<?php include_once('app-footer.php'); ?>