<!DOCTYPE html>
<html lang="en">
<head>
<?php
	session_start(); 
    if(!$_SESSION){ header("location:../index.php"); }
    
    spl_autoload_register( function( $NombreClase ) {
        require_once '../classes/' . $NombreClase . '.php';
    });
?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="../assets/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
<div class="container-fluid">