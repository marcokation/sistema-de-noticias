<?php include_once('app-header.php'); ?>

<h1>
   <form method="post">
        Bienvenido <span class="text-danger"><?php echo $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name']; ?></span>! 
        <a href="inicio.php" class="btn btn-secondary">Home</a>
        <input type="submit" class="btn btn-secondary" value="Cerrar sesion" name="salir" id="salir">
   </form>

   <?php 
    if(isset($_POST['salir'])){
        $cn = new ConexionnMySQL;
        $cn->closeSession();
    }

    $slug = $_GET['slug'];
    $nt = new Noticia;
    $noticia = $nt->showNoticia($slug);
   ?>
</h1>
<hr>
<h2  class="shadow-none p-3 mb-5 bg-light rounded text-uppercase">Actualizar noticia - <span class="text-dark"><?php echo $noticia['titulo']; ?></span></h2>
<form method="POST" action="../classes/noticia.php">
    <input type="hidden" name="slug" value="<?php echo $noticia['slug']; ?>">
    <div class="form-row">
        <div class="col-md-6">
            <label for="titulo">Titulo del libro*</label>
            <input type="text" name="titulo" class="form-control" id="titulo" aria-describedby="emailHelp" required value="<?php echo $noticia['titulo']; ?>" placeholder="Ingresar titulo">
            <small id="d-titulo" style="color:red; display:none;">El campo email es requerido</small>
        </div>
        <div class="col-md-6">
            <label for="autor">Autor*</label>
            <input type="text" name="autor" class="form-control" id="autor" aria-describedby="autor" required value="<?php echo $noticia['autor']; ?>" placeholder="Nombre del autor de la noticia">
            <small id="d-autor" style="color:red; display:none;">El campo email es requerido</small>
        </div>
    </div>
    <br>
    <div class="form-row">
        <div class="col-md-12">
            <label for="contenido">Contenido*</label>
            <textarea name="contenido" class="form-control" id="contenido" value="" cols="30" required rows="10"><?php echo $noticia['contenido']; ?></textarea>
            <small id="d-contenido" style="color:red; display:none;">El campo email es requerido</small>
        </div>
    </div>
    <br>
    <button type="submit" id="noticiaSubmit" class="btn btn-success btn-lg btn-block">Actualizar</button>
</form>

<?php include_once('app-footer.php'); ?>