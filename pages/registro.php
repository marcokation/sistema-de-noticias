<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        session_start();
        if($_SESSION){ header("location:pages/inicio.php"); }

        spl_autoload_register( function( $NombreClase ) {
            require_once '../classes/' . $NombreClase . '.php';
        });
    ?>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <form id="registro">
        <input type="text" name="name" id="name" value="" placeholder="Nombre"><br>
        <small id="d-name" style="color:red; display:none;">El campo nombre es requerido</small><br>
        <input type="text" name="lastname" id="lastname" value="" placeholder="Apellido"><br>
        <small id="d-lastname" style="color:red; display:none;">El campo apellido es requerido</small><br>
        <input type="text" name="email" id="email" value="" placeholder="Email"><br>
        <small id="d-email" style="color:red; display:none;">El campo email es requerido</small><br>
        <input type="password" name="password" value="" id="password" placeholder="Password"><br>
        <small id="d-pass" style="color:red; display:none;">El campo password es requerido</small><br>
        <button type="button" id="registrarme">Registrarme</button>
    </form>
    <p>¿Ya tienes una cuenta? <a href="../index.php">Login</a></p>

 <script type="text/javascript">

    $(document).ready(function(){
        $('#registrarme').click(function(){

            if($('#name').val() != '' && $('#lastname').val() != '' && $('#email').val() != '' && $('#password').val() != ''){
            
                let cadena = "name=" + $('#name').val() +
                        "&lastname=" + $('#lastname').val() +
                        "&email=" + $('#email').val() +
                        "&password=" + $('#password').val();
                $.ajax({
                    type:"POST",
                    url:"../classes/registro.php",
                    data:cadena,
                    success:function(){
                        alert('Registro exitoso');
                        $('#name').val('');
                        $('#lastname').val('');
                        $('#email').val('');
                        $('#password').val('');
                    },
                });
            }else{
                alert('Llenar todos los campos para completar el registro.');
            }
        });
    });        
    </script>

<?php include_once('app-footer.php'); ?>