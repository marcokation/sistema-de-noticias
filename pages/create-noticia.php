<?php include_once('app-header.php'); ?>

<h1>
   <form method="post">
        Bienvenido <span class="text-danger"><?php echo $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name']; ?></span>!
        <a href="inicio.php" class="btn btn-secondary">Home</a>
        <input type="submit" class="btn btn-secondary" value="Cerrar sesion" name="salir" id="salir">
   </form>

   <?php 
   if(isset($_POST['salir'])){
      $close = new ConexionnMySQL;
      $close->closeSession();
   }
   ?>
</h1>
<hr>
<h2  class="shadow-none p-3 mb-5 bg-light rounded text-uppercase">Agregar nueva noticia</h2>
<form method="POST" action="../classes/Noticia.php">
    <input type="hidden" name="userId" value="<?php echo $_SESSION['user']['id']; ?>">
    <div class="form-row">
        <div class="col-md-6">
            <label for="titulo">Titulo del libro*</label>
            <input type="text" name="titulo" class="form-control" id="titulo" aria-describedby="emailHelp" value="" required placeholder="Ingresar titulo">
            <small id="d-titulo" style="color:red; display:none;">El campo email es requerido</small>
        </div>
        <div class="col-md-6">
            <label for="autor">Autor*</label>
            <input type="text" name="autor" class="form-control" id="autor" aria-describedby="autor" value="" required placeholder="Nombre del autor de la noticia">
            <small id="d-autor" style="color:red; display:none;">El campo email es requerido</small>
        </div>
    </div>
    <br>
    <div class="form-row">
        <div class="col-md-12">
            <label for="contenido">Contenido*</label>
            <textarea name="contenido" class="form-control" id="contenido" value="" required cols="30" rows="10"></textarea>
            <small id="d-contenido" style="color:red; display:none;">El campo email es requerido</small>
        </div>
    </div>
    <br>
    <button type="submit" class="btn btn-success btn-lg btn-block">Publicar</button>
</form>

<?php include_once('app-footer.php'); ?>